﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace AWESBannersProviderAPI
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "BannerAPI",
                routeTemplate: "api/CATEGORY/{Category}/TYPE/{Type}/BANNER/{id}",
                defaults: new { controller = "banner", id = RouteParameter.Optional }
            );
        }
    }
}
