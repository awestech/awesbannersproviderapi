﻿using AWESBannersProviderAPIModel.BannersCategory;
using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Data;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage.Blob;
using AWESBannersProviderAPIModel.BannersImage;

namespace AWESBannersProviderAPI.Controllers
{
    public class bannerController : baseController
    {
        /// <summary>
        /// get method which returns the existing banners
        /// </summary>
        /// <param name="Category">category name in the admin</param>
        /// <param name="Type">type of webpage in the admin</param>
        /// <returns>object with information of the banners</returns>
        public async Task<IHttpActionResult> Get(string Category, string Type)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            try
            {
                string lang = GetQueryString("lang");
                string size = GetQueryString("size");

                if(string.IsNullOrEmpty(lang) | string.IsNullOrEmpty(size)){
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }

                if(size=="L"){
                    var bannersES = await (from i in dataContext.banners
                                           where i.category == Category &&
                                           i.type == Type &&
                                           (i.language == "es" || i.language.Substring(0,2)== "es")
                                           select i).Distinct().ToListAsync();
 
                    var banners = await (from i in dataContext.banners
                                         where i.category==Category &&
                                         i.type==Type &&
                                         i.language==lang
                                         select i).Distinct().ToListAsync();

                    List<string> urlLanguageAsked = (from i in banners select i.urlLarge).ToList();
                    foreach (var bannerES in bannersES)
                    {
                        //URL's from language asked
                        if (!urlLanguageAsked.Contains(bannerES.urlLarge)) {
                            banners.Add(bannerES);
                        }
                    }

                    return Ok(banners);
                }
                if (size == "M")
                {
                    var bannersES = await (from i in dataContext.banners
                                           where i.category == Category &&
                                           i.type == Type &&
                                           (i.language == "es" || i.language.Substring(0, 2) == "es")
                                           select i).Distinct().ToListAsync();

                    var banners = await (from i in dataContext.banners
                                         where i.category == Category &&
                                         i.type == Type &&
                                         i.language == lang
                                         select i).Distinct().ToListAsync();

                    List<string> urlLanguageAsked = (from i in banners select i.urlMedium).ToList();
                    foreach (var bannerES in bannersES)
                    {
                        //URL's from language asked
                        if (!urlLanguageAsked.Contains(bannerES.urlMedium))
                        {
                            banners.Add(bannerES);
                        }
                    }

                    return Ok(banners);
                }
                if (size == "S")
                {
                    var bannersES = await (from i in dataContext.banners
                                           where i.category == Category &&
                                           i.type == Type &&
                                           (i.language == "es" || i.language.Substring(0, 2) == "es")
                                           select i).Distinct().ToListAsync();

                    var banners = await (from i in dataContext.banners
                                         where i.category == Category &&
                                         i.type == Type &&
                                         i.language == lang
                                         select i).Distinct().ToListAsync();

                    List<string> urlLanguageAsked = (from i in banners select i.urlSmall).ToList();
                    foreach (var bannerES in bannersES)
                    {
                        //URL's from language asked
                        if (!urlLanguageAsked.Contains(bannerES.urlSmall))
                        {
                            banners.Add(bannerES);
                        }
                    }

                    return Ok(banners);
                }
                await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                return BadRequest();
            }
            catch (Exception ex)
            {
                await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        /// <summary>
        /// get method by ID, not implemented.
        /// </summary>
        /// <param name="Category"></param>
        /// <param name="Type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Get(string Category, string Type,int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// method post of banners
        /// </summary>
        /// <param name="value">contain the information for add banner</param>
        /// <returns>return a status code noContent or badrequest if value is null</returns>
        public async Task<IHttpActionResult> Post([FromBody]banner value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.banners.Add(value);
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        /// <summary>
        /// method for edit banners, not implemented.
        /// </summary>
        /// <param name="id">ID of banner</param>
        /// <param name="value">contain the information for edit banner</param>
        /// <returns>return a status code noContent</returns>
        public async Task<IHttpActionResult> Put(int id, [FromBody]banner value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.Entry(value).State = EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        /// <summary>
        /// method delelte of banners
        /// </summary>
        /// <param name="id">ID of banner</param>
        /// <returns>return a status code noContent</returns>
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            try
            {
                dataContext.banners.Remove(dataContext.banners.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

    }
}
