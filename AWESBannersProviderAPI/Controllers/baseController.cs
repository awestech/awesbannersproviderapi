﻿using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AWESBannersProviderAPI.Controllers
{
    public class baseController : AWESAPIComunes.API.AWESBaseAPIController<AWESBannersProviderAPIModel.dataContext>
    {
        public static string platform { get { return System.Configuration.ConfigurationManager.AppSettings["platform"]; } }
        public static string CDNName { get { return ""; } }
        public static string CDNFileUrl { get { return "https://awesimagestorage.blob.core.windows.net/xmlfiles/"; } }

        #region Protected functions

        protected CloudStorageAccount RetrieveStorageAccount(string StorageConnectionString = "")
        {
            CloudStorageAccount storageAccount;
            //USE A DEFAULT CONNECTION STRING IF IT IS NOT ESPECIFIED
            if (string.IsNullOrEmpty(StorageConnectionString))
            {
                storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            }
            // USE AN ESPECIFIC CONNECTION STRING
            else
            {
                storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting(StorageConnectionString));
            }
            return storageAccount;
        }
        protected CloudBlobClient RetrieveBlobClient(string StorageConnectionString = "")
        {
            CloudBlobClient blobClient = null;
            if (string.IsNullOrEmpty(StorageConnectionString))
                blobClient = RetrieveStorageAccount().CreateCloudBlobClient();
            else
                blobClient = RetrieveStorageAccount(StorageConnectionString).CreateCloudBlobClient();

            return blobClient;
        }
        protected CloudFileClient RetrieveFileClient(string StorageConnectionString = "")
        {
            CloudFileClient blobClient = null;
            if (string.IsNullOrEmpty(StorageConnectionString))
                blobClient = RetrieveStorageAccount().CreateCloudFileClient();
            else
                blobClient = RetrieveStorageAccount(StorageConnectionString).CreateCloudFileClient();

            return blobClient;
        }
        protected CloudBlobContainer RetrieveContainer(string containerName, string StorageConnectionString = "")
        {
            CloudBlobContainer container = null;
            CloudBlobClient blobClient = null;
            if (string.IsNullOrEmpty(StorageConnectionString))
                blobClient = RetrieveBlobClient();
            else
                blobClient = RetrieveBlobClient(StorageConnectionString);
            if (!string.IsNullOrEmpty(containerName))
                container = blobClient.GetContainerReference(containerName);
            else
                return null;

            return container;
        }

        #endregion
    }
}