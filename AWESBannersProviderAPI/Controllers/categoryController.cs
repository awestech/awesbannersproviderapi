﻿using AWESBannersProviderAPIModel.BannersCategory;
using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace AWESBannersProviderAPI.Controllers
{
    public class categoryController : baseController
    {
        // GET categories
        public async Task<IHttpActionResult> Get()
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            try
            {
                
                var categories = await (from i in dataContext.categories
                                        select new { id = i.ID + "", name = i.name, description = i.description }).Distinct().ToListAsync();
                return Ok(categories);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        // GET category by ID
        public async Task<IHttpActionResult> Get(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var category = await (from i in dataContext.categories
                                      where i.ID == id
                                      select i).FirstAsync();
                return Ok(category);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        // POST category
        public async Task<IHttpActionResult> Post([FromBody]category value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.categories.Add(value);
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        // PUT category
        public async Task<IHttpActionResult> Put(int id, [FromBody]category value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                dataContext.Entry(value).State = EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }

        // DELETE category
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);
            try
            {
                dataContext.categories.Remove(dataContext.categories.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
