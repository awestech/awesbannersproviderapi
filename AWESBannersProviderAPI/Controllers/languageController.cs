﻿using AWESCommon.Configuration;
using AWESCommon.statsSupport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Data;
using AWESBannersProviderAPIModel.BannersLanguage;

namespace AWESBannersProviderAPI.Controllers
{
    public class languageController : baseController
    {
        /// <summary>
        /// method get of languages
        /// </summary>
        /// <returns>return list of de languages with their order</returns>
        public async Task<IHttpActionResult> Get()
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var languages = await (from i in dataContext.languages
                                       select new { lang = i.lang + (i.locale == "" ? "" : "-" + i.locale), order = i.order }).GroupBy(m => m.lang)
                                       .Select(m => m.FirstOrDefault())
                                       .OrderBy(i => i.order)
                                       .ToListAsync();
                return Ok(languages);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        /// <summary>
        /// method get of laguage
        /// </summary>
        /// <param name="id">ID of language</param>
        /// <returns>information of the language</returns>
        public async Task<IHttpActionResult> Get(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                var languages = await (from i in dataContext.languages
                                       where i.ID == id
                                       select new { lang = i.lang + (i.locale == "" ? "" : "-" + i.locale) }).ToListAsync();
                return Ok(languages);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        /// <summary>
        /// method post of languages
        /// </summary>
        /// <param name="value">contain the information for add language</param>
        /// <returns>return a status code noContent</returns>
        public async Task<IHttpActionResult> Post([FromBody]language value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                value.lang = value.lang.ToLower();
                value.locale = value.locale.ToUpper();
                dataContext.languages.Add(value);
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }

        }
        /// <summary>
        /// method put of languages
        /// </summary>
        /// <param name="id">not implemented</param>
        /// <param name="value">contain the information for edit language</param>
        /// <returns>return a status code noContent</returns>
        public async Task<IHttpActionResult> Put(int id, [FromBody]language value)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                if (value == null)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                if (value.lang.Length > 2 || value.locale.Length > 2)
                {
                    await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".BadRequest"));
                    return BadRequest();
                }
                value.lang = value.lang.ToLower();
                value.locale = value.locale.ToUpper();
                dataContext.Entry(value).State = EntityState.Modified;
                await dataContext.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                await statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
        /// <summary>
        /// method delete of languages
        /// </summary>
        /// <param name="id">ID of Language</param>
        /// <returns>return a status code noContent</returns>
        public async Task<IHttpActionResult> Delete(int id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            try
            {
                dataContext.languages.Remove(dataContext.languages.Find(id));
                await dataContext.SaveChangesAsync();
                return Ok(HttpStatusCode.NoContent);
            }
            catch (Exception ex)
            {
                statsHelper.statAsync(new statsEntry(functionalUnit, callingMethod(), callingContext + ".Error"));
                throw new Exception(callingContext + "::" + ex.Message);
            }
        }
    }
}
