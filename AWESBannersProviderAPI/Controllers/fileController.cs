﻿using AWESCommon.bannerSupport;
using AWESCommon.Configuration;
using Microsoft.Azure;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using static AWESCommon.bannerSupport.bannerHelper;

namespace AWESBannersProviderAPI.Controllers
{
    public class fileController : baseController
    {
        /// <summary>
        /// Not implemented
        /// </summary>
        /// <returns>Not implemented</returns>
        public async Task<IHttpActionResult> Get()
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            //QUERYSTRINGS
            string TrackCode = GetQueryString("TrackCode");
            string Tablename = GetQueryString("Tablename");

            //PRIMARY VALIDATIONS
            if (string.IsNullOrEmpty(TrackCode) && string.IsNullOrEmpty(Tablename)) {
                return Ok("search commands are empty.");
            }

            string shareNameReference = configurationReader.read("shareNameReference", "AWESAdminDashboard");

            CloudFileClient fileClient = RetrieveFileClient();
            CloudFileDirectory cloudFileDirectory = fileClient.GetShareReference(shareNameReference).GetRootDirectoryReference().GetDirectoryReference("logs");

            List<CloudFile> listFiles = new List<CloudFile>();
            foreach (var item in cloudFileDirectory.ListFilesAndDirectories())
            {
                if (item.GetType().Name == "CloudFile")
                {
                    listFiles.Add((CloudFile)item);
                }
            }

            if (!string.IsNullOrEmpty(TrackCode))
            {
                listFiles = listFiles.Where(a => a.Name.Contains(TrackCode)).ToList();
            }
            if (!string.IsNullOrEmpty(Tablename))
            {
                listFiles = listFiles.Where(a => a.Name.Contains(Tablename)).ToList();
            }

            List<byte[]> listFileBytes = new List<byte[]>();
            foreach (CloudFile file in listFiles)
            {
                await file.FetchAttributesAsync();
                byte[] arrTarget = new byte[file.Properties.Length];
                file.DownloadToByteArray(arrTarget, 0);
                listFileBytes.Add(arrTarget);
            }

            return Ok(listFileBytes);
        }

        // GET: api/file/5
        public async Task<IHttpActionResult> Get(string id)
        {
            string marketingSignatureName = configurationReader.read("Consume.marketingSignatureName", platform);
            string marketingSignature = configurationReader.read("Consume.marketingSignature", platform);

            validateSignature(marketingSignatureName, marketingSignature);

            return Ok("Not implemented");
        }

        /// <summary>
        /// Post files to CDN awesimagesstorage
        /// </summary>
        /// <param name="value">dictionary((trackcode,table),xml)</param>
        /// <returns>List of URL's</returns>
        public async Task<IHttpActionResult> Post([FromBody]List<POCOXmldocuments> value)
        {
            string shareNameReference = configurationReader.read("shareNameReference", "AWESAdminDashboard");

            CloudBlobContainer blobContainer = null;
            blobContainer = RetrieveContainer(shareNameReference.ToLower());
            
            List<string> urlsToSendBack = new List<string>();

            //GetQueryString("");

            foreach (POCOXmldocuments item in value)
            {
                //NAME CREATION
                string path = Path.GetTempPath();
                string nameFile;
                if (!item.uniqueFile)
                    nameFile = item.trackCode + "_" + DateTime.Now.Year + "_" + DateTime.Now.Month 
                    + "_" + DateTime.Now.Day +
                     "_" + item.tableName + ".txt";
                else
                    nameFile = item.trackCode + "_" + item.tableName + ".txt";
                

                string urlToPost = Path.Combine(path, nameFile);

                try
                {
                    CloudBlockBlob blobBlock = blobContainer.GetBlockBlobReference(nameFile);
                    using (StreamWriter sw = File.CreateText(urlToPost))
                    {
                        sw.WriteLine(item.xmlDoc);
                    }
                    await blobBlock.UploadFromFileAsync(urlToPost);

                    File.Delete(urlToPost);
                }
                catch (Exception ex)
                {
                    Ok(ex);
                }

                urlsToSendBack.Add(CDNFileUrl + nameFile);
            }
            return Ok(urlsToSendBack);
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Put(string id, [FromBody]string value)
        {
            return Ok();
        }

        /// <summary>
        /// Not implemented
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IHttpActionResult> Delete(string id)
        {
            return Ok();
        }

        private CloudStorageAccount RetrieveStorageAccount(string StorageConnectionString = "")
        {
            CloudStorageAccount storageAccount;
            //USE A DEFAULT CONNECTION STRING IF IT IS NOT ESPECIFIED
            if (string.IsNullOrEmpty(StorageConnectionString))
            {
                storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            }
            // USE AN ESPECIFIC CONNECTION STRING
            else
            {
                storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting(StorageConnectionString));
            }
            return storageAccount;
        }
        private CloudBlobClient RetrieveBlobClient(string StorageConnectionString = "")
        {
            CloudBlobClient blobClient = null;
            if (string.IsNullOrEmpty(StorageConnectionString))
                blobClient = RetrieveStorageAccount().CreateCloudBlobClient();
            else
                blobClient = RetrieveStorageAccount(StorageConnectionString).CreateCloudBlobClient();

            return blobClient;
        }
        private CloudBlobContainer RetrieveContainer(string containerName, string StorageConnectionString = "")
        {
            CloudBlobContainer container = null;
            CloudBlobClient blobClient = null;
            if (string.IsNullOrEmpty(StorageConnectionString))
                blobClient = RetrieveBlobClient();
            else
                blobClient = RetrieveBlobClient(StorageConnectionString);
            if (!string.IsNullOrEmpty(containerName))
                container = blobClient.GetContainerReference(containerName);
            else
                return null;

            return container;
        }
    }
}
