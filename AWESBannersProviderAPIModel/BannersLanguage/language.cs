﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESBannersProviderAPIModel.BannersLanguage
{
    public class language
    {
        public int ID { get; set; }

        [Display(Name = "language")]
        [MaxLength(2, ErrorMessage = "Max length is 2")]
        public string lang { get; set; }

        [Display(Name = "locale")]
        [MaxLength(2, ErrorMessage = "Max length is 2")]
        public string locale { get; set; }

        [Display(Name = "order")]
        public int order { get; set; }
    }
}
