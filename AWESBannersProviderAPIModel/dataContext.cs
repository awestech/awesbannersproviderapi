﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AWESBannersProviderAPIModel.BannersCategory;
using AWESBannersProviderAPIModel.BannersImage;
using AWESBannersProviderAPIModel.BannersLanguage;

namespace AWESBannersProviderAPIModel
{
    public class dataContext : DbContext
    {
        public dataContext() : base("defaultConnectionString") { }
        public dataContext(string connString) : base(connString) { }
        public DbSet<category> categories { get; set; }
        public DbSet<banner> banners { get; set; }
        public DbSet<language> languages { get; set; }
    }
}
