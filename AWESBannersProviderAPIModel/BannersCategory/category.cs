﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESBannersProviderAPIModel.BannersCategory
{
    public class category
    {
        public int ID { get; set; }

        [Display(Name = "Category name")]
        [MaxLength(63, ErrorMessage = "Max length is 63")]
        [MinLength(3, ErrorMessage = "Min length is 3")]
        public string name { get; set; }
        [Display(Name = "Category description")]
        [MaxLength(512, ErrorMessage = "Max length is 512")]
        public string description { get; set; }
    }
}
