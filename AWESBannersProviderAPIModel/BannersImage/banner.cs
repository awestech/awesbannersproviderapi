﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AWESBannersProviderAPIModel.BannersImage
{
    public class banner
    {
        public int ID { get; set; }

        [Display(Name = "Banner name")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string name { get; set; }
        [Display(Name = "Banner category")]
        [MaxLength(63, ErrorMessage = "Max length is 63")]
        [MinLength(3, ErrorMessage = "Min length is 3")]
        public string category { get; set; }
        [Display(Name = "Banner type")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string type { get; set; }
        [Display(Name = "Altern text")]
        [MaxLength(100, ErrorMessage = "Max length is 100")]
        public string alternText { get; set; }
        [Display(Name = "Banner language")]
        [MaxLength(5, ErrorMessage = "Max length is 5")]
        public string language { get; set; }
        [Display(Name = "Banner title")]
        [MaxLength(50, ErrorMessage = "Max length is 50")]
        public string title { get; set; }
        [Display(Name = "Large URL")]
        [MaxLength(200, ErrorMessage = "Max length is 200")]
        public string urlLarge { get; set; }
        [Display(Name = "Medium URL")]
        [MaxLength(200, ErrorMessage = "Max length is 200")]
        public string urlMedium { get; set; }
        [Display(Name = "Large URL")]
        [MaxLength(200, ErrorMessage = "Max length is 200")]
        public string urlSmall { get; set; }
    }
}
